import React, { useState } from 'react';
import Product from "../../components/Product/Product";
import styles from "./ProductsAll.module.scss";
import PropTypes from "prop-types";

const ProductsAll = (props) => {
  const [favoriteArr, setFavoriteArr] = useState(JSON.parse(localStorage.getItem('favoriteArr')) || []);

  const changeFavoriteArr = (id) => {
    const index = favoriteArr.indexOf(id);
    const newFavoriteArr = [...favoriteArr];
    if(index !== -1) {
      newFavoriteArr.splice(index,1);
    } else {
      newFavoriteArr.push(id);
    }
    setFavoriteArr(newFavoriteArr);
    localStorage.setItem('favoriteArr', JSON.stringify(newFavoriteArr));
  }

    const {products, incrementCartCount, incrementFavoriteCount} = props;
    return (
      <>
        <h1 className={styles.ProductsAllTitle}>Latest arrivals in KitGoods</h1>
        <div className={styles.ProductsAll}>
          {products.map(({ id, isFavorite, price, currency, article, ...args }) => (
            <Product
                incrementCartCount={incrementCartCount}
                incrementFavoriteCount={incrementFavoriteCount}
                key={id}
                id={parseInt(id)}
                price={parseFloat(price)}
                currency="$"
                article={parseInt(article)}
                changeFavoriteArr={changeFavoriteArr}
                isFavorite={favoriteArr.indexOf(id) !== -1}
                {...args}
            />
          ))}
        </div>
      </>
    );
}

ProductsAll.propTypes = {
  products: PropTypes.array,
  incrementCartCount: PropTypes.func,
  incrementFavoriteCount: PropTypes.func,

};

ProductsAll.defaultProps = {
  products: [],
  incrementCartCount: () => {},
  incrementFavoriteCount:  () => {}
};

export default ProductsAll;