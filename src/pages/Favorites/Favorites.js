import React, { useState } from 'react';
import FavoriteItem from "../../components/FavoriteItem/FavoriteItem";
import styles from "./Favorite.module.scss";
import PropTypes from "prop-types";

const Favorites = (props) => {
    const [favoriteArr, setFavoriteArr] = useState(JSON.parse(localStorage.getItem('favoriteArr')) || []);

    const changeFavoriteArr = (id) => {
        const index = favoriteArr.indexOf(id);
        const newFavoriteArr = [...favoriteArr];
        if(index !== -1) {
            newFavoriteArr.splice(index,1);
        } else {
            newFavoriteArr.push(id);
        }
        setFavoriteArr(newFavoriteArr);
        localStorage.setItem('favoriteArr', JSON.stringify(newFavoriteArr));
    }

    const {products, incrementCartCount, incrementFavoriteCount} = props;
    const productsInFavorites = products.filter(el => favoriteArr.includes(el.id))

    return (
        <>
            <h1 className={styles.FavoritesTitle}>Your Favorites</h1>
            <div className={styles.Favorites}>
                {productsInFavorites.map(({ id, isFavorite, price, currency, article, ...args }) => (
                    <FavoriteItem
                        incrementCartCount={incrementCartCount}
                        incrementFavoriteCount={incrementFavoriteCount}
                        key={id}
                        id={parseInt(id)}
                        price={parseFloat(price)}
                        currency="$"
                        article={parseInt(article)}
                        changeFavoriteArr={changeFavoriteArr}
                        isFavorite={favoriteArr.indexOf(id) !== -1}
                        {...args}
                    />
                ))}
            </div>
        </>
    );
}

Favorites.propTypes = {
    products: PropTypes.array,
    incrementCartCount: PropTypes.func,
    incrementFavoriteCount: PropTypes.func
};

Favorites.defaultProps = {
    products: [],
    incrementCartCount: () => {},
    incrementFavoriteCount:  () => {}
};

export default Favorites;