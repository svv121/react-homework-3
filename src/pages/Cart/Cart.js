import React from 'react';
import CartItem from "../../components/CartItem/CartItem";
import styles from "./Cart.module.scss";
import PropTypes from "prop-types";

const Cart = (props) => {

    const cartArr = JSON.parse(localStorage.getItem('cartArr')) || [];
    const {products, incrementCartCount} = props;
    const productsInCart = products.filter(el => cartArr.includes(el.id))

    return (
        <>
            <h1 className={styles.CartTitle}>Your Cart</h1>
            <div className={styles.Cart}>
                {productsInCart.map(({ id, price, currency, article, ...args }) => (
                    <CartItem
                        incrementCartCount={incrementCartCount}
                        key={id}
                        id={parseInt(id)}
                        price={parseFloat(price)}
                        currency="$"
                        article={parseInt(article)}
                        {...args}
                    />
                ))}
            </div>
        </>
    );
}

Cart.propTypes = {
    products: PropTypes.array,
    incrementCartCount: PropTypes.func
};

Cart.defaultProps = {
    products: [],
    incrementCartCount: () => {},
};

export default Cart;