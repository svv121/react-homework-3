import React, { useState } from 'react';
import styles from "./CartItem.module.scss";
import PropTypes from "prop-types";
import Button from "../Button/Button";
import Modal from "../Modal/Modal";

const CartItem = (props) => {
    const [isOpenCartModal, setIsOpenCartModal] = useState(false);
    const [cartArr, setCartArr] = useState(JSON.parse(localStorage.getItem('cartArr')) || []);

    const openCartModal = () => {
        setIsOpenCartModal(true)
    };

    const deleteFromCart = (id) => {
        const newCartArr = (JSON.parse(localStorage.getItem('cartArr')) || []).filter(el => el !== id);
        setCartArr(newCartArr);
        localStorage.setItem('cartArr', JSON.stringify(newCartArr));
        const productInCartCount = {};
        newCartArr.forEach(item => {productInCartCount[item] = (productInCartCount[item] || 0) + 1});
        localStorage.setItem('cartObj', JSON.stringify(productInCartCount));
    }

    const {
        id,
        name,
        price,
        currency,
        url,
        article,
        color,
        incrementCartCount
    } = props;

    return (
        <div className={styles.case}>
            <div className={styles.deleteFromCart}>
            <Button
                handleClick={openCartModal}
                backgroundColor="#F07465"
                text="&#9587;"
                id={id}
            />
            </div>
            {
                isOpenCartModal &&
                <Modal
                header="Deleting this product from your cart"
                closeButton = {true}
                text={`You are about to delete: ${name} from your cart`}
                LeftBtnTxt="Delete"
                RightBtnTxt="Cancel"
                bgColor="#F07465"
                closeModal={() => {
                    setIsOpenCartModal(false)
                }}
                actions={() => {
                    incrementCartCount(-(JSON.parse(localStorage.getItem('cartArr'))).filter(el => el === id).length);
                    deleteFromCart(id);
                    setIsOpenCartModal(false);
                }}
                />
            }
            <div className={styles.productImgWrapper}>
                <img src={url} alt={name} className={styles.productImg} />
            </div>
            <h2 className={styles.name}>{name}</h2>
            <div className={styles.priceWrapper}>
                <h4 className={styles.price}>Price: {currency}{price}</h4>
                <div className={styles.countInfo}>
                    <p>Quantity: {+JSON.parse(localStorage.getItem('cartArr')).filter(el => el === id).length}</p>
                </div>
            </div>
            <p>Article: {article}</p>
            <p>Color: {color}</p>
        </div>
    );
}

CartItem.propTypes = {
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    name: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    url: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    currency: PropTypes.string.isRequired,
    article: PropTypes.number.isRequired,
    color: PropTypes.string.isRequired,
    incrementCartCount: PropTypes.func,
};

CartItem.defaultProps = {
    incrementCartCount: () => {},
};

export default CartItem;