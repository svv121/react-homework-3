import React, { useState } from 'react';
import styles from "./Product.module.scss";
import PropTypes from "prop-types";
import Button from "../Button/Button";
import Modal from "../Modal/Modal";
import { ReactComponent as StarEmpty } from "../../assets/svg/star-empty.svg";
import { ReactComponent as StarFilled } from "../../assets/svg/star-filled.svg";

const Product = (props) => {
  const [isOpenCartModal, setIsOpenCartModal] = useState(false);
  const [cartArr, setCartArr] = useState(JSON.parse(localStorage.getItem('cartArr')) || []);

 const openCartModal = () => {
     setIsOpenCartModal(true)
  };

 const addToCart = (id) => {
    const newCartArr = JSON.parse(localStorage.getItem('cartArr')) || [];
    newCartArr.push(id);
    setCartArr(newCartArr);
    localStorage.setItem('cartArr', JSON.stringify(newCartArr));
    const productInCartCount = {};
    newCartArr.forEach(item => {productInCartCount[item] = (productInCartCount[item] || 0) + 1});
    localStorage.setItem('cartObj', JSON.stringify(productInCartCount));
  }

    const {
      id,
      name,
      price,
      currency,
      url,
      article,
      color,
      isFavorite,
      changeFavoriteArr,
      incrementCartCount,
      incrementFavoriteCount
       } = props;
    return (
      <div className={styles.case}>
        <div onClick={() => changeFavoriteArr(id)}>
          {isFavorite?
              <StarFilled onClick={() => incrementFavoriteCount(-1)} className={styles.star}/> :
              <StarEmpty onClick={() => incrementFavoriteCount(1)} className={styles.star}/>
          }
        </div>
        <div className={styles.productImgWrapper}>
          <img src={url} alt={name} className={styles.productImg} />
        </div>
        <h2 className={styles.name}>{name}</h2>
        <div className={styles.priceWrapper}>
          <h4 className={styles.price}>Price: {currency}{price}</h4>

          <Button
              handleClick={openCartModal}
              backgroundColor="#64B743"
              text="Add to cart"
              id={id}
          />
          {
              isOpenCartModal &&
              <Modal
                  header="Adding this product to your cart"
                  closeButton = {true}
                  text={`Once you add this product: ${name}, you can continue your purchases or you can checkout`}
                  LeftBtnTxt="Add"
                  RightBtnTxt="Cancel"
                  bgColor="#64B743"
                  closeModal={() => {
                      setIsOpenCartModal(false)
                  }}
                  actions={() => {
                    incrementCartCount(1);
                    addToCart(id);
                    setIsOpenCartModal(false);
                  }}
              />
          }
        </div>
        <p>Article: {article}</p>
        <p>Color: {color}</p>
      </div>
    );
}

Product.propTypes = {
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  name: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  url: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  currency: PropTypes.string.isRequired,
  article: PropTypes.number.isRequired,
  color: PropTypes.string.isRequired,
  isFavorite: PropTypes.bool,
  changeFavoriteArr: PropTypes.func,
  incrementCartCount: PropTypes.func,
  incrementFavoriteCount: PropTypes.func,
};

Product.defaultProps = {
  isFavorite: false,
  changeFavoriteArr:  () => {},
  incrementCartCount: () => {},
  incrementFavoriteCount:  () => {}
};

export default Product;