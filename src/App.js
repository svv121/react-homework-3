import './App.css';
import React, { useState, useEffect } from 'react';
import Header from "./components/Header/Header";
import Routing from "./routing/Routing";
import PropTypes from "prop-types";

function App() {
    const [productsData, setProductsData] = useState([]);
    const [cartCount, setCartCount] = useState(0);
    const [favoriteCount, setFavoriteCount] = useState(0);

    const incrementCartCount = (num) => {
        localStorage.setItem('cartCount', JSON.stringify(cartCount + num));
        return setCartCount(cartCount + num)
    }

    const incrementFavoriteCount = (num) => {
        localStorage.setItem('favoriteCount', JSON.stringify(favoriteCount + num));
        return setFavoriteCount(favoriteCount + num)
    }

    useEffect(() => {
        (async () => {
            const productsDataInLocalStorage = localStorage.getItem("productsData");
            const productsData = JSON.parse(productsDataInLocalStorage);
            if (productsData) {
                setProductsData(productsData);
            } else {
                const result = await fetch("./products/products.json").then(res => res.json());
                localStorage.setItem("productsData", JSON.stringify(result))
                setProductsData(result)
            }
        })();
        if (localStorage.getItem('cartArr')) {
            setCartCount(+JSON.parse(localStorage.getItem('cartArr')).length)
        } else {setCartCount(0)}
        if (localStorage.getItem('favoriteArr')) {
            setFavoriteCount(+JSON.parse(localStorage.getItem('favoriteArr')).length)
        } else {setFavoriteCount(0)}
    }, []);

    return (
        <div className="App">
            <Header
                favoriteCount={favoriteCount}
                cartCount={cartCount}
            />
            <Routing
                products={productsData}
                incrementCartCount={incrementCartCount}
                incrementFavoriteCount={incrementFavoriteCount}
            />
        </div>
    );
}

App.propTypes = {
    productsData: PropTypes.array
};

App.defaultProps = {
    data: []
};

export default App;